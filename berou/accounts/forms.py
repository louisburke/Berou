# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth import get_user_model


class RegisterUserForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'email']

    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean_password2(self):
        super(RegisterUserForm, self).clean()
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 != password2:
            raise forms.ValidationError('The two passwords are different')
        return password1

    def save(self, *args, **kwargs):
        self.full_clean()
        self.instance.set_password(self.cleaned_data['password1'])
        self.instance.save(*args, **kwargs)
        return self.instance
