from django.conf import settings
from django.contrib.auth import (
    login, authenticate, get_user_model, forms as django_forms
)

from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.translation import ugettext as _
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, FormView, UpdateView
from django.contrib.auth.forms import PasswordChangeForm

from accounts import forms
from berou import mixins


class RegisterUserView(mixins.LoginExcludedMixin, CreateView):
    model = get_user_model()
    template_name = 'accounts/register_user.html'
    form_class = forms.RegisterUserForm

    success_url = settings.LOGIN_REDIRECT_URL

    def form_valid(self, form):
        response = super(RegisterUserView, self).form_valid(form)
        user = authenticate(username=form.cleaned_data['username'],
                            password=form.cleaned_data['password1'])
        login(self.request, user)
        return response


class LoginUserView(FormView):
    form_class = django_forms.AuthenticationForm
    template_name = 'accounts/login_user.html'
    success_url = settings.LOGIN_REDIRECT_URL

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(reverse('berou:home'))
        return super(LoginUserView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        query_string = self.request.META['QUERY_STRING']
        if not query_string or not query_string.startswith('next='):
            return self.success_url
        return query_string.split('next=')[1]

    def form_valid(self, form):
        response = super(LoginUserView, self).form_valid(form)
        login(self.request, form.get_user())
        return response

    def get_form(self, form_class=None):
        form = super(LoginUserView, self).get_form(form_class)
        form.fields['username'].widget.attrs['placeholder'] = _('Username')
        form.fields['password'].widget.attrs['placeholder'] = _('Password')
        return form


class ProfileMixin(object):
    context_object_name = 'user_object'


class ProfileView(mixins.LoginRequiredMixin, ProfileMixin, DetailView):
    model = get_user_model()
    template_name = 'accounts/profile.html'


class EditProfileView(ProfileMixin, UpdateView):
    model = get_user_model()
    template_name = 'accounts/edit_profile.html'
    fields = ['first_name', 'last_name', 'email']

    def get_success_url(self):
        return reverse('accounts:profile', kwargs={'pk': self.object.id})


class ChangePasswordView(mixins.LoginRequiredMixin, FormView):
    form_class = PasswordChangeForm
    template_name = 'accounts/change_password.html'

    def get_form(self, form_class):
        if self.request.method == 'POST':
            return form_class(user=self.request.user, data=self.request.POST)
        return form_class(user=self.request.user)

    def get_success_url(self):
        return reverse('accounts:profile', kwargs={'pk': self.request.user.id})

    def form_valid(self, form):
        """ Saves the form, and relogs the user in. """
        user = self.request.user
        new_password = form.cleaned_data['new_password1']
        response = super(ChangePasswordView, self).form_valid(form)
        form.save()
        user = authenticate(username=user.username, password=new_password)
        login(self.request, user)
        return response
