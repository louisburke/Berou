from django.conf import settings
from django.conf.urls import url

from accounts import views

urlpatterns = [
    url(r'^$', views.RegisterUserView.as_view(), name='register_user'),
    url(r'^login/$',
        views.LoginUserView.as_view(),
        name='login_user'),
    url(r'^logout/$', 'django.contrib.auth.views.logout',
        {'next_page': settings.LOGOUT_REDIRECT_URL},
        name='logout_user'),
    url(r'^profile/(?P<pk>\d+)$',
        views.ProfileView.as_view(),
        name='profile'),
    url(r'^edit_profile/(?P<pk>\d+)$',
        views.EditProfileView.as_view(),
        name='edit_profile'),
    url(r'^change_password/$',
        views.ChangePasswordView.as_view(),
        name='change_password'),
]
