#!/usr/bin/env python
# -*- coding: utf-8 -*-

from accounts import views


def header(request):
    login_view = views.LoginUserView()
    login_view.request = request
    context = {
        'login_form': login_view.get_form(),
    }
    return context
