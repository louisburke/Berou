# -*- coding: utf-8 -*-

from django import template

register = template.Library()


@register.filter
def partition(thelist, n):
    if len(thelist) < n:
        return [thelist]
    new_list = []
    sublist = []
    for i, item in enumerate(thelist):
        sublist.append(item)
        if (i + 2) % n == 1 or (i + 1) == len(thelist):
            new_list.append(sublist)
            sublist = []
    return new_list
