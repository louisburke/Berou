# -*- coding: utf-8 -*-

from django.conf.urls import url

from berou import views

urlpatterns = [
    url(r'^$', views.ListAppView.as_view(), name='home')
]
