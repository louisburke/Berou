import importlib
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from accounts import urls as accounts_urls
from berou import berou_urls
from application_system import urls as application_system_urls
from application_system import models


def get_plugin_packages():
    return [app.package for app in models.App.objects.all()]


urlpatterns = [
    # Examples:
    # url(r'^$', 'berou.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include(accounts_urls, namespace='accounts')),
    url(r'^application_system/', include(application_system_urls,
                                         namespace='application_system')),
    url(r'^', include(berou_urls, namespace='berou')),
]

for plugin in get_plugin_packages():
    has_urls = True
    try:
        urls_module = importlib.import_module(plugin + '.urls')
    except ImportError:
        has_urls = False
    if has_urls:
        plugin_urls = url(
            r'^{plugin}/'.format(plugin=plugin),
            include(urls_module, namespace=plugin)
        )
        urlpatterns.append(plugin_urls)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
