#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.shortcuts import redirect


class OwnershipRequiredMixin(object):
    """Changes the queryset to contain only the user's links"""

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user).all()


class LoginRequiredMixin(object):
    """Ensures that user must be authenticated in order to access view."""

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class LoginExcludedMixin(object):
    """Ensures that user must be authenticated in order to access view."""

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            return redirect('berou:home')
        return super(LoginExcludedMixin, self).dispatch(*args, **kwargs)