# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.shortcuts import redirect, render
from django.views.generic.list import ListView

from application_system import models
from berou import mixins


class ListAppView(mixins.OwnershipRequiredMixin, ListView):
    model = models.App.users.through

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            super(ListAppView, self).get(request, *args, **kwargs)
            return render(
                request,
                'berou/home.html',
                self.get_context_data(**kwargs)
            )
        return redirect(reverse('accounts:register_user'))
