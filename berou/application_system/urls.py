from django.conf.urls import url

from application_system import views

urlpatterns = [
    url(r'^$', views.ListAppView.as_view(), name='list_app'),
    url(r'^list_available_app/$',
        views.ListAvailableAppView.as_view(),
        name='list_available_app'),
    url(r'^install_app/$', views.InstallAppView.as_view(), name='install_app'),
    url(r'^uninstall_app/(?P<package>[\w-]+)/$',
        views.UninstallAppView.as_view(),
        name='uninstall_app'),
    url(r'activate_app/(?P<package>[\w-]+)/$',
        views.ActivateAppView.as_view(),
        name='activate_app'),
    url(r'disable_app/(?P<package>[\w-]+)/$',
        views.DisableAppView.as_view(),
        name='disable_app'),
]
