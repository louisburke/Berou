# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0007_app_users'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='app',
            options={'verbose_name': 'Application', 'verbose_name_plural': 'Applications'},
        ),
        migrations.AlterField(
            model_name='app',
            name='description',
            field=models.CharField(default=b'', max_length=200, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='app',
            name='name',
            field=models.CharField(max_length=200, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='app',
            name='slug',
            field=models.SlugField(unique=True, max_length=200, verbose_name='Slug'),
        ),
        migrations.AlterField(
            model_name='app',
            name='users',
            field=models.ManyToManyField(default=None, to=settings.AUTH_USER_MODEL, verbose_name='Users'),
        ),
    ]
