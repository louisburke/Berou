# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0002_auto_20150504_0113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='app',
            name='installed',
        ),
        migrations.AlterField(
            model_name='app',
            name='name',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]
