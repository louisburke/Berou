# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0008_auto_20150530_2205'),
    ]

    operations = [
        migrations.AddField(
            model_name='app',
            name='logo',
            field=models.CharField(help_text='Path to the logo', max_length=200, null=True, verbose_name='Logo'),
        ),
    ]
