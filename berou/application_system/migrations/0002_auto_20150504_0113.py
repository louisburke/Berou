# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='app',
            name='installed',
            field=models.BooleanField(default=True),
        ),
    ]
