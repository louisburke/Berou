#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger('berou')


class App(models.Model):
    class Meta:
        verbose_name = _('Application')
        verbose_name_plural = _('Applications')

    name = models.CharField(max_length=200, verbose_name=_('Name'))
    description = models.CharField(
        max_length=200,
        default='',
        verbose_name=_('Description')
    )
    package = models.CharField(
        max_length=200,
        unique=True,
        verbose_name=_('Package name'),
        default=None
    )
    logo = models.CharField(
        max_length=200,
        verbose_name=_('Logo'),
        help_text=_('Path to the logo'),
        null=True,
    )
    description = models.TextField(verbose_name=_('Description'))
    users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        default=None,
        verbose_name=_('Users')
    )

    def get_absolute_url(self):
        return reverse('application_system:list_app')

    def __str__(self):
        string_length = 20
        if len(self.name) > string_length:
            name = self.name[:string_length-1] + '...'
        else:
            name = self.name
        return u'<App: name="{name}">'.format(name=name)

    def __eq__(self, instance):
        return instance.slug == self.slug

    def save(self, *args, **kwargs):
        app = App.objects.filter(package=self.package).first()
        if app:
            logger.info('App already installed')
            self.id = app.id
        return super(App, self).save(*args, **kwargs)
