#!/usr/bin/env python
# -*- coding: utf-8 -*-

import importlib

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        description = (
            'The app name you want to install. '
            'Must have been installed with setup.py or pip'
        )
        parser.add_argument('app_name', help=description)

    def handle(self, *args, **options):
        app = options.get('app_name')
        module = importlib.import_module(app + '.install')
        module.install()
