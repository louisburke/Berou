from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from berou import mixins
from application_system import models
#from berou import settings as project_settings


class ListAppView(ListView):
    model = models.App
    template_name = 'application_system/list_app.html'


class InstallAppView(mixins.LoginRequiredMixin, CreateView):
    model = models.App
    fields = ['name']
    success_url = reverse_lazy('application_system:list_app')


class UninstallAppView(mixins.LoginRequiredMixin, DeleteView):
    model = models.App
    template_name = 'application_system/confirm_uninstall_app.html'
    success_url = reverse_lazy('application_system:list_app')


class ListAvailableAppView(mixins.LoginRequiredMixin, ListAppView):
    template_name = 'application_system/list_available_app.html'
    model = models.App

    def get_context_data(self, **kwargs):
        context = super(ListAvailableAppView, self).get_context_data(**kwargs)
        context['installed_apps'] = [app for app in models.App.objects.all()
                                     if self.request.user in app.users.all()]
        return context


class ActivateAppView(mixins.LoginRequiredMixin, UpdateView):
    model = models.App
    fields = []
    slug_field = 'package'
    slug_url_kwarg = 'package'
    success_url = reverse_lazy('application_system:list_available_app')

    def get_object(self, queryset=None):
        obj = super(ActivateAppView, self).get_object(queryset)
        return obj

    def form_valid(self, form):
        form.instance.users.add(self.request.user)
        return super(ActivateAppView, self).form_valid(form)


class DisableAppView(mixins.LoginRequiredMixin, UpdateView):
    model = models.App
    fields = []

    success_url = reverse_lazy('application_system:list_available_app')

    def form_valid(self, form):
        form.instance.users.remove(self.request.user)
        return super(DisableAppView, self).form_valid(form)
