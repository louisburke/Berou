========
Tutorial
========

In this tutorial, we will create a single page, with a single url.
This tutorial assumes you have Bérou installed.
The Django project is located in berou,
and all the commands show in this tutorial are run from this directory.
::

    cd berou


Create a Django app
===================
::

    ./manage.py startapp my_app

Urls
====

In order to display your first page, you will need to create some URLs.
Create a `urls.py` in your app directory.
::

    from django.conf.urls import url

    from my_app import views

    urlpatterns = [
        url(r'^$', views.MyView.as_view(), name='my_page'),
    ]


The important line is the second to last line.
It means: When the user is trying to access the root page, use MyView.
I want to use this URL later (in a template for example), so I call it 'my_page'.

Now that we have our first URL, we need to create `MyView` in the views module.

Views
=====

Create a `views.py` in your app directory.
::

    from django.views.generic import TemplateView


    class MyView(TemplateView):
        template_name = 'my_app/my_template.html'


Templates
=========

The base template
-----------------

The base template is the template you need to inherit from in your own templates.
It comes with the structure of the page. The structure works as follow:
::

    <!DOCTYPE html>
    <html>
    <head>
        <link rel="stylesheet" href="bootstrap.css">
        <script src="bootstrap.css"></script>
    </head>
    <body>
        <div class="header">
            header content
        </div>
        <div class="container-fluid">
            {% block content %}
            {% endblock content %}
        </div>
        <footer>
            footer content
        </footer>
    </body>
    </html>

Note that this is just some pseudo code that is here to explain the structure and not the real code.
If you want to look what it looks like, look at: `berou/berou/templates/berou/base.html`.

As you may have noticed, Bérou uses `bootstrap <http://getbootstrap.com/>`_ for its CSS.

Your first template
-------------------

`templates/my_app/my_template.html`
::

    {% extends 'berou/base.html' %}
    {% load i18n %}
    {% block content %}
    <div class="row">
        {% trans "Hello world!" %}
    </div>
    {% endblock content %}


If you don't understand what the `{% trans "Hello world!" %}` is for, I invite you to read the main page.

After doing this, you can install your app in Bérou.
To do so, create a file called `install.py` in your app folder, with the following content:
::

    # -*- coding: utf-8 -*-

    from django.utils.translation import ugettext_lazy as _
    from django.templatetags.static import static

    from application_system import models


    def install():
        app = models.App(
            name=__package__.capitalize(),
            description=_('My first app'),
            package=__package__,
        )
        app.save()

Once you have saved this file, install it like this:
::

    ./manage.py install_app my_app

See the result
==============

Start the server:
::

    ./manage.py runserver

Create your user account
------------------------

Go to http://localhost:8000/ in your browser, and register a new account.

In your browser, go to: http://localhost:8000, activate `My_app`
and click on it in the header of the page.

When you have created your account, you are invited to activate an app.
Activate your newly installed app, and click on its name
in the header bar to see your work.
