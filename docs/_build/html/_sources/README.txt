=====
Bérou
=====

Bérou is a platform that comes built-in with a repository
of applications ready to work on it.

Installation
============

::

    # Get the source code
    git clone git@gitlab.com:Pacodastre/Berou.git

    # Create a virtualenv for the project
    mkvirtualenv berou

    # cd into the project
    cd Berou

    # Install dependencies using pip
    pip install -r berou/requirements.txt

    # cd into the django project
    cd berou

    # Create the database
    ./manage.py migrate

After installing the software, you might to create a superuser to access the admin interface.

::

    ./manage.py createsuperuser




Running the project
===================

Start the server with the following command:

::

    ./manage.py runserver


Create your app
===============

To create an app, just run this command:
::

    ./manage.py startapp <app name>  # Replace <app name> with the actual name of your app

A Bérou application needs an extra file, for now, you can copy this template and modify the values.
Create a file called `install.py` in `berou/<app name>` with this content:::


    # -*- coding: utf-8 -*-

    from django.utils.translation import ugettext_lazy as _
    from django.templatetags.static import static

    from application_system import models


    def install():
        app = models.App(
            name=__package__.capitalize(),
            description=_('Describe your app here'),
            slug=__package__,
            logo=static('path to your logo'),  # if you don't have a logo, just remove this line
        )
        app.save()


At the moment, all the apps are using `Django Class-Based Views <https://docs.djangoproject.com/en/1.8/topics/class-based-views/>`_.

For a complete tutorial on how to create django apps, we invite you to read the `official tutorial <https://docs.djangoproject.com/en/1.8/intro/tutorial01/>`_.

`A good reference for Django Class-Based Views <http://ccbv.co.uk/>`_


Install applications
====================

To install applications that you are developing, you simply need to run this command:::

    ./manage.py install_app <app name>


If you wish to install an app from the Bérou pypi project, you can install them like this:::

    pip install <app name> --index-url=http://pypi.berou.fr
    ./manage.py install_app <app name>

That's it!

The Bérou pypi server
=====================

A public instance of pypiserver is available here: http://pypi.berou.fr/
If you want to see all the available packages that you can install with the command above,
just go to: http://pypi.berou.fr/simple


How to upload your code to http://pypi.berou.fr
===============================================

TODO

Deploy Bérou
============

Bérou comes with configuration files for all the tools
you need to set up in order to Berou up and running in the clouds.

Here is the list of applications that Bérou needs:

- nginx: the web server. It will deliver the pages generated by Bérou to your users' browsers.
- `gunicorn <http://gunicorn.org/>`_: a `wsgi <http://en.wikipedia.org/wiki/Web_Server_Gateway_Interface>`_ server.
  It does the communication between nginx and Bérou.
- `supervisor <http://supervisord.org/>`_: a Process Control System. It will allow you to easily start/stop Bérou.


Configure nginx
---------------

Bérou comes with an nginx configuration file. You just need to fix the different paths.
The paths should be very closed to yours. When you're done fixing the paths, the next thing
to do is to create a symlink in `/etc/nginx/sites-available/`,
and then make another symlink from the file in sites-available.
The symlink needs to be located in `/etc/nginx/sites-enabled/`

The nginx configuration file in Bérou is located in `Bérou/deployment/nginx/`. Rename the file if needed.

::

    # Assuming you're at the root of the Bérou project
    ln -s deployment/nginx/berou.fr /etc/nginx/sites-available/
    ln -s /etc/nginx/sites-available/ /etc/nginx/sites-enabled/

`sites-available` is the folder that contains all sites served by nginx.
They are not necessarily activated and accessible. Only the ones contained in `sites-enabled` are activated.
If you wish to disable Bérou, just remove the file in `sites-enabled`.

When you're done with the nginx configuration, reload its conf:
::

    service nginx reload


Configure gunicorn and supervisor
---------------------------------

2 files needs to be edited with your settings.

`deployment/start_server.sh` is a shell script that starts gunicorn. Fix the value of `DJANGODIR`.

`deployment/supervisor/supervisord.conf` is the configuration file for supervisor.
You will to edit the following settings:

- `file`, in the `unix_http_server` section. The path is likely to be wrong.
  The `run` directory is located at the root of Bérou.
- `logfile`, in the `supervisord` section. The `log` directory is located at the root of Bérou.
- `pidfile`, in the `supervisord` section.
- `serverurl`, in the `supervisorctl` section. Note that it needs to match the `file` settings.
- `directory`, `command`, `stdout_logfile` in the `program:berou` section.

Start Bérou
-----------

That's it for the configuration. You can now start the server like this:
::

    supervisord -c deployment/supervisor/supervisord.conf

Now that supervisor has been started, you can send it commands:
::

    supervisorctl restart berou
    supervisorctl stop berou
    supervisorctl start berou
    supervisorctl shutdown  # shutdowns supervisord

How to translate my app ?
=========================

In order to make your app available in different languages, you need to do a few things:

The first things you need to do is create a `locale` directory in your app folder.
When the directory is create, you will need to wrap all your strings with the `ugettext` function.

Use `{% trans %}` in templates
------------------------------

In order to use the ugettext function, you need to load the `i18n` module. Then, you need to use the
`trans` templatetag with your strings as parameters.

Exemple:
::

    {% extends 'berou/base.html' %}
    {% load i18n %}
    {% trans 'Hello world!' %}
    {% block content %}
    {% endblock content %}

Use `ugettext` in python files
------------------------------

`views.py`
::

    from django.utils.translation import ugettext as _
    from django.views.generic import TemplateView

    class MyView(TemplateView):
        template_name = 'my_app/my_template.html'

        def get_context_data(self, **kwargs):
            context = super(MyView, self).get_context_data(**kwargs)
            context['my_text'] = _('Hello world!')
            return context

Run `makemessages` to create `.po` files
----------------------------------------

When you have at least one string wrapped in a `ugettext` call,
you can run `makemessages` to create a `.po` file.
`makemessages` needs a language code in order to create the right file.

This is how you run it:
::

    django-admin makemessages -l fr

Run this command in your app folder. `fr` is for the French language.
You can replace `fr` with any language that you can find on the
wikipedia page related called `List of ISO 639-1 codes <http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes>`_

When running this command, Django searches in your application for translatable strings.
It will collect all of them and will write them in a `.po` file.
Empty strings will also be created for you to fill in with your translation.
The result in located in `locale/fr/LC_MESSAGES/django.po`.

When you're done translating strings, you need to compile your translations:
::


    ./manage.py compilemessages

That's it, your application is now translated.
If a french user accesses Bérou, they will get the text in French.
If HTTP requests have the `accept-language` header, django will use its value
in order to determine which language to use.
There is an article published on `w3 <http://www.w3.org/International/questions/qa-accept-lang-locales>`_.
