=====
Bérou
=====

Bérou is a platform that comes built-in with a repository
of applications ready to work on it.


.. toctree::
   :maxdepth: 2

   tutorial
   i18n
   deployment


Installation
============

::

    # Get the source code
    git clone https://gitlab.com/Pacodastre/Berou.git

    # Create a virtualenv for the project
    mkvirtualenv berou

    # cd into the project
    cd Berou

    # Install dependencies using pip
    pip install -r berou/requirements.txt

    # cd into the django project
    cd berou

    # Create the database
    ./manage.py migrate

After installing the software, you might to create a superuser to access the admin interface.

::

    ./manage.py createsuperuser


Running the project
===================

Start the server with the following command:

::

    ./manage.py runserver


About Bérou apps
================

At the moment, all the apps in the Bérou are using `Django Class-Based Views <https://docs.djangoproject.com/en/1.8/topics/class-based-views/>`_.

For a complete tutorial on how to create django apps, we invite you to read the `official tutorial <https://docs.djangoproject.com/en/1.8/intro/tutorial01/>`_.

`A good reference for Django Class-Based Views <http://ccbv.co.uk/>`_


Install applications
====================

To install applications that you are developing, you simply need to run this command:::

    ./manage.py install_app <app name>


If you wish to install an app from the Bérou pypi project, you can install them like this:::

    pip install <app name> --index-url=http://pypi.berou.fr
    ./manage.py install_app <app name>

That's it!

The Bérou pypi server
=====================

A public instance of pypiserver is available here: http://pypi.berou.fr/
If you want to see all the available packages that you can install with the command above,
just go to: http://pypi.berou.fr/simple


How to upload your code to http://pypi.berou.fr
===============================================

TODO

.. _tutorial: tutorial
