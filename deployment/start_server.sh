#!/bin/bash
source ~/.bashrc

NAME="berou"  # Name of the application
DJANGODIR=/home/paco/Projects/Berou/berou
SOCKFILE=$DJANGODIR/../run/berou.fr.sock  # we will communicte using this unix socket
USER=$USER  # the user to run as
GROUP=$USER  # the group to run as
NUM_WORKERS=3  # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=berou.settings  # which settings file should Django use
DJANGO_WSGI_MODULE=berou.wsgi                     # WSGI module name

workon berou

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
echo $DJANGO_WSGI_MODULE
cd $DJANGODIR
gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER \
  --group=$GROUP \
  --log-level=debug \
  --bind=unix:$SOCKFILE \
  --pid $HOME/gunicorn.pid \
  --access-logfile $DJANGODIR/../log/gunicorn.access.log \
  --error-logfile $DJANGODIR/../log/gunicorn.error.log
